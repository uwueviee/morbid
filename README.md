# morbid

## Prelude

I don't really consider myself paranoid, I don't believe there's some group who wants me dead, but I am worried about what my friends and family are going to think of me after I die.

I wanted a way to be able to send an encrypted document to people using various messaging platforms if I've been away from the internet for a set amount of time.
Doing research, I didn't really like any of the current solutions available as I need multiple platforms and even some more obscure platform support.

As such, I've created this, the morbid protocol and server. A tiny and easy to use dead drop that can virtually support any platform that allows text and can run on most networks.

I want this to be something that can be very adaptive, and as such, I would like to support as many situations as possible. If you feel that something is missing, I encourage you to make an issue or even a pull request.

## Rust Server

## Protocol

See protocol/README.md